# État de la traduction (vehicles)

 * **changé**: 49
 * **libre**: 8


Dernière mise à jour: 2024-03-04 08:06 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0H2BbJCAKauXYKE6.htm](vehicles/0H2BbJCAKauXYKE6.htm)|Sand Barge|Barge des sables|changé|
|[1ldIHIoe1aKHilX2.htm](vehicles/1ldIHIoe1aKHilX2.htm)|Sandsailer|Voilier des sables|changé|
|[2dLy1sfqVbNS2inJ.htm](vehicles/2dLy1sfqVbNS2inJ.htm)|Firefly|Luciole électrique|changé|
|[66dAXEMUjL7BrX8I.htm](vehicles/66dAXEMUjL7BrX8I.htm)|Carriage|Carriole|changé|
|[6CFijrzNSXtS8ExJ.htm](vehicles/6CFijrzNSXtS8ExJ.htm)|Sky Chariot (Light)|Chariot céleste léger|changé|
|[7ye7abxWAFdVbATr.htm](vehicles/7ye7abxWAFdVbATr.htm)|Chariot (Heavy)|Chariot lourd|changé|
|[8rXCF2Fr4iA0VmFx.htm](vehicles/8rXCF2Fr4iA0VmFx.htm)|Sand Racer|Bolide de sable|changé|
|[bUwBenWXjdUIKXkm.htm](vehicles/bUwBenWXjdUIKXkm.htm)|Clockwork Borer|Foreur mécanique|changé|
|[cmLOaqR3qebmLA1P.htm](vehicles/cmLOaqR3qebmLA1P.htm)|Clockwork Bumblebee|Bourdon mécanique|changé|
|[Cr4T0TcaxMr3bUaj.htm](vehicles/Cr4T0TcaxMr3bUaj.htm)|Speedster|Bolide|changé|
|[cvP9ytBY9SJbaNbo.htm](vehicles/cvP9ytBY9SJbaNbo.htm)|Apparatus of the Octopus|Appareil pieuvre|changé|
|[Cx53sUC5FZ3GZT0O.htm](vehicles/Cx53sUC5FZ3GZT0O.htm)|Raft|Radeau|changé|
|[d4sKpXnX6k2fHduY.htm](vehicles/d4sKpXnX6k2fHduY.htm)|Battery Tower|Tour Batterie|changé|
|[FbSRhHvCmXBSVkrn.htm](vehicles/FbSRhHvCmXBSVkrn.htm)|Steam Cart|Chariot à vapeur|changé|
|[fiSvQBWyyJuXnVvn.htm](vehicles/fiSvQBWyyJuXnVvn.htm)|Automated Cycle|Vélo automatisé|changé|
|[fUJ1RB9l9DppWdCH.htm](vehicles/fUJ1RB9l9DppWdCH.htm)|Titanic Stomper|Piétineur titanique|changé|
|[GMoxs9RQ88uMxfW4.htm](vehicles/GMoxs9RQ88uMxfW4.htm)|Bathysphere|Bathysphère|changé|
|[HA93YJ0qjmRk0Nh8.htm](vehicles/HA93YJ0qjmRk0Nh8.htm)|Galley|Galère|changé|
|[IhniEmlnkb3pFRZG.htm](vehicles/IhniEmlnkb3pFRZG.htm)|Clockwork Hopper|Bondisseur mécanique|changé|
|[iItKvPymPNMUaGY7.htm](vehicles/iItKvPymPNMUaGY7.htm)|Hot Air Balloon|Montgolfière|changé|
|[JQAknpXhJo5bwgOM.htm](vehicles/JQAknpXhJo5bwgOM.htm)|Strider|Glisseur|changé|
|[jrLiGrOwgOhr83PL.htm](vehicles/jrLiGrOwgOhr83PL.htm)|Clockwork Castle|Château mécanique|changé|
|[K0ThJViHQzzxRmqE.htm](vehicles/K0ThJViHQzzxRmqE.htm)|Second Kiss|Second baiser|changé|
|[l0RfYgabnbNoHnkS.htm](vehicles/l0RfYgabnbNoHnkS.htm)|Rowboat|Bâteau à rames|changé|
|[MCU4ZiNNAX7GQNJb.htm](vehicles/MCU4ZiNNAX7GQNJb.htm)|Firework Pogo|Pogo feu d'artifice|changé|
|[mq8bsMnrY81gUEYb.htm](vehicles/mq8bsMnrY81gUEYb.htm)|Cart|Charrette|changé|
|[MrGgl8HDfQYITahL.htm](vehicles/MrGgl8HDfQYITahL.htm)|Clockwork Wagon|Carrosse mécanique|changé|
|[nnFXTqngaq5k1V7C.htm](vehicles/nnFXTqngaq5k1V7C.htm)|Longship|Drakkar|changé|
|[oHEqb4rOoZUzBmkH.htm](vehicles/oHEqb4rOoZUzBmkH.htm)|Helepolis|Hélépolis|changé|
|[oq6khbojns3YRi2g.htm](vehicles/oq6khbojns3YRi2g.htm)|Cliff Crawler|Chenille de falaise|changé|
|[Qlvei5jjRXdsXUPo.htm](vehicles/Qlvei5jjRXdsXUPo.htm)|Cutter|Cotre|changé|
|[QMPVAWtaClZtD2Ip.htm](vehicles/QMPVAWtaClZtD2Ip.htm)|Shark Diver|Requin plongeur|changé|
|[R2ga1eyeSQA9w6KF.htm](vehicles/R2ga1eyeSQA9w6KF.htm)|Sailing Ship|Bâteau à voile|changé|
|[RGEmINZa7YXvbruu.htm](vehicles/RGEmINZa7YXvbruu.htm)|Clunkerjunker|Casse-bagnole|changé|
|[slkeDT68Zjmzz5l5.htm](vehicles/slkeDT68Zjmzz5l5.htm)|Steam Trolley|Carriole à vapeur|changé|
|[t57KE8cj9MdG6Y5H.htm](vehicles/t57KE8cj9MdG6Y5H.htm)|Bone Ship|Vaisseau squelettique|changé|
|[tVBApIWuwKNwOics.htm](vehicles/tVBApIWuwKNwOics.htm)|Steam Giant|Géant à vapeur|changé|
|[UNOwkQq5lc4cYdZP.htm](vehicles/UNOwkQq5lc4cYdZP.htm)|Wagon|Carrosse|changé|
|[vbJL4dPQcXfYigwv.htm](vehicles/vbJL4dPQcXfYigwv.htm)|Sky Chariot (Medium)|Chariot celeste moyen|changé|
|[VNlJ7zPnOPThFEmR.htm](vehicles/VNlJ7zPnOPThFEmR.htm)|Airship|Navire volant|changé|
|[VRhdfL78HS5sT4gF.htm](vehicles/VRhdfL78HS5sT4gF.htm)|Snail Coach|Coche escargot|changé|
|[vU3UnKWCRFhnvNLV.htm](vehicles/vU3UnKWCRFhnvNLV.htm)|Ambling Surveyor|Enquêteur ambulant|changé|
|[w6auGNQQ2VyMT001.htm](vehicles/w6auGNQQ2VyMT001.htm)|Sand Diver|Plongeur des sables|changé|
|[WjOkOaWOGkLmuSq5.htm](vehicles/WjOkOaWOGkLmuSq5.htm)|Armored Carriage|Carriole blindée|changé|
|[WWvqHSAYOw4ysn3e.htm](vehicles/WWvqHSAYOw4ysn3e.htm)|Sleigh|Traineau|changé|
|[Y2XrhWFdiGcVAUsz.htm](vehicles/Y2XrhWFdiGcVAUsz.htm)|Sky Chariot (Heavy)|Chariot céleste lourd|changé|
|[ySR1Ds607BSX1tCw.htm](vehicles/ySR1Ds607BSX1tCw.htm)|Adaptable Paddleboat|Bâteau à rames ajustable|changé|
|[yukamyipO8x9VuZa.htm](vehicles/yukamyipO8x9VuZa.htm)|Siege Tower|Tour de siège|changé|
|[yXbxHtz3xVIMbxyh.htm](vehicles/yXbxHtz3xVIMbxyh.htm)|Mobile Inn|Auberge mobile|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[7uVEeLDVMfneyyZ5.htm](vehicles/7uVEeLDVMfneyyZ5.htm)|Sled|Traineau|libre|
|[bMgQftCEiSqa1pTk.htm](vehicles/bMgQftCEiSqa1pTk.htm)|Vonthos's Golden Bridge|Passerelle d'or de Vonthos|libre|
|[KC1QdoG0mLi9Lzol.htm](vehicles/KC1QdoG0mLi9Lzol.htm)|Bunta|Bunta|libre|
|[oyKhe8J0h8G9IHE3.htm](vehicles/oyKhe8J0h8G9IHE3.htm)|Glider|Planeur|libre|
|[V6ZuxECTGSyHAm7h.htm](vehicles/V6ZuxECTGSyHAm7h.htm)|Hillcross Glider|Planeur des collines|libre|
|[WBMeO6BPqEzc6Yv8.htm](vehicles/WBMeO6BPqEzc6Yv8.htm)|Chariot (Light)|Chariot léger|libre|
|[WsfgcVXqqmk1EVRY.htm](vehicles/WsfgcVXqqmk1EVRY.htm)|Velocipede|Vélocipède|libre|
|[WXwFZ3DrLe2PchNf.htm](vehicles/WXwFZ3DrLe2PchNf.htm)|Cauldron of Flying|Chaudron de vol|libre|
