# État de la traduction (malevolence-bestiary)

 * **libre**: 20


Dernière mise à jour: 2024-03-04 08:06 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1wb9Vi3V4GDeOBCt.htm](malevolence-bestiary/1wb9Vi3V4GDeOBCt.htm)|Ixirizmid|Ixirizmid|libre|
|[1XrwqxEW8rUr8238.htm](malevolence-bestiary/1XrwqxEW8rUr8238.htm)|Tanglebones|Oschevêtré|libre|
|[8zR1Gmv6rIlsWsTR.htm](malevolence-bestiary/8zR1Gmv6rIlsWsTR.htm)|Xarwin Caul|Chrysalide de Xarwin|libre|
|[bEFdPjTLtNboaTg2.htm](malevolence-bestiary/bEFdPjTLtNboaTg2.htm)|Esobok Ghoul|Goule Ésobok|libre|
|[ecdfItOKsZVc4v0L.htm](malevolence-bestiary/ecdfItOKsZVc4v0L.htm)|Xarwin Portraits|Les portraits de Xarwin|libre|
|[etVnNsUFpgeOBFJO.htm](malevolence-bestiary/etVnNsUFpgeOBFJO.htm)|Carrion Vortex|Vortex de charogne|libre|
|[fQClrtmAe3AER0w2.htm](malevolence-bestiary/fQClrtmAe3AER0w2.htm)|Lured to Ashes|Attiré dans les cendres|libre|
|[Gywa8GSJtDCPSjbt.htm](malevolence-bestiary/Gywa8GSJtDCPSjbt.htm)|Architect and Hunter|L'architecte et le chasseur|libre|
|[JMXNSC7M2SRGZbhy.htm](malevolence-bestiary/JMXNSC7M2SRGZbhy.htm)|Undead Brain Collector|Collecteur de cerveaux mort-vivant|libre|
|[n3taMFiViyEQ5JAi.htm](malevolence-bestiary/n3taMFiViyEQ5JAi.htm)|Ioseff Xarwin|Ioseff Xarwin|libre|
|[sdpB7n2LbzVRqrnv.htm](malevolence-bestiary/sdpB7n2LbzVRqrnv.htm)|Yianyin|Yianyin|libre|
|[TrX8Op3qZzFvX4Df.htm](malevolence-bestiary/TrX8Op3qZzFvX4Df.htm)|Mouth of Tchekuth|Bouche de Tchekuth|libre|
|[vcFMNQf9vg7r11ca.htm](malevolence-bestiary/vcFMNQf9vg7r11ca.htm)|Xarwin's Manifestation|Manifestation de Xarwin|libre|
|[VgBE6ow0CvsQxFkq.htm](malevolence-bestiary/VgBE6ow0CvsQxFkq.htm)|Algea|Algéa|libre|
|[w2EmOacJ2zGfygFu.htm](malevolence-bestiary/w2EmOacJ2zGfygFu.htm)|Wrathful Hatchet|Hachette furieuse|libre|
|[Wkn2wZbsNSifAnBh.htm](malevolence-bestiary/Wkn2wZbsNSifAnBh.htm)|Anitoli Nostraema|Anitoli Nostraema|libre|
|[x0RGlIHXDN9y0D08.htm](malevolence-bestiary/x0RGlIHXDN9y0D08.htm)|Corrupted Nosoi|Nosoi corrompu|libre|
|[Y9GX6pST1P9Y3Okh.htm](malevolence-bestiary/Y9GX6pST1P9Y3Okh.htm)|Haunted Nosoi|Nosoi hanté|libre|
|[YSL27Oq2xFIzrq9u.htm](malevolence-bestiary/YSL27Oq2xFIzrq9u.htm)|Nils Kelveken|Nils Kelveken|libre|
|[ZhxhWosYW3tZfDqL.htm](malevolence-bestiary/ZhxhWosYW3tZfDqL.htm)|Xarwin Manor Phantasm|Le fantasme du manoir de Xarwin|libre|
