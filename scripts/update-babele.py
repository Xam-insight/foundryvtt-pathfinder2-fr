#!/usr/bin/python3
# -*- coding: utf-8 -*-

from libdata import *

BABELE = "../babele/vf/fr/"
BABELE_VF_VO = "../babele/vf-vo/fr/"
BABELE_VO_VF = "../babele/vo-vf/fr/"
BABELE_VO = "../babele/vo/fr/"


def handlePack(p, babeleVf, babeleVfVo, babeleVoVf, babeleVo):
    path = "../data/%s/" % p["id"]
    if not os.path.isdir(path):
        os.mkdir(path)
    all_files = os.listdir(path)

    count = {"aucune": 0, "libre": 0, "officielle": 0, "changé": 0, "vide": 0}

    # add mappings
    if 'desc' in p:
        babeleVf['mapping']["description"] = p['desc']
        babeleVfVo['mapping']["description"] = p['desc']
        babeleVoVf['mapping']["description"] = p['desc']
        babeleVo['mapping']["description"] = p['desc']
    if 'gm' in p:
        babeleVf['mapping']["gm"] = p['gm']
        babeleVfVo['mapping']["gm"] = p['gm']
        babeleVoVf['mapping']["gm"] = p['gm']
        babeleVo['mapping']["gm"] = p['gm']
    if "fields" in p:
        for k in p["fields"]:
            babeleVf['mapping'][k.lower()] = p["fields"][k]
            babeleVfVo['mapping'][k.lower()] = p["fields"][k]
            babeleVoVf['mapping'][k.lower()] = p["fields"][k]
            babeleVo['mapping'][k.lower()] = p["fields"][k]
    if "lists" in p:
        for k in p["lists"]:
            babeleVf['mapping'][k.lower()] = p["lists"][k]
            babeleVfVo['mapping'][k.lower()] = p["lists"][k]
            babeleVoVf['mapping'][k.lower()] = p["lists"][k]
            babeleVo['mapping'][k.lower()] = p["lists"][k]

    # Ajout des converters Babele
    if "items" in p:
        babeleVf['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}
        babeleVfVo['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}
        babeleVoVf['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}
        babeleVo['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}

    if "subMappings" in p:
        for submapping_key, submapping_value in p["subMappings"].items():
            babeleVf["mapping"][submapping_key] = {"path": submapping_value["base"], "converter": submapping_value["converter"]}
            babeleVfVo["mapping"][submapping_key] = {"path": submapping_value["base"], "converter": submapping_value["converter"]}
            babeleVoVf["mapping"][submapping_key] = {"path": submapping_value["base"], "converter": submapping_value["converter"]}
            babeleVo["mapping"][submapping_key] = {"path": submapping_value["base"], "converter": submapping_value["converter"]}
    if "id" in p and p["id"] == "equipment":
        babeleVf["mapping"]["name"] = {"path": "name", "converter": "translateEquipmentName"}
        babeleVfVo["mapping"]["name"] = {"path": "name", "converter": "translateEquipmentName"}
        babeleVoVf["mapping"]["name"] = {"path": "name", "converter": "translateEquipmentName"}
        babeleVo["mapping"]["name"] = {"path": "name", "converter": "translateEquipmentName"}

    # read all files in folder
    for fpath in all_files:
        # _folders.json
        if "_folders.json" in fpath:
            with open(path + fpath, 'r', encoding='utf8') as f:
                babeleVf["folders"] = {}
                babeleVfVo["folders"] = {}
                babeleVoVf["folders"] = {}
                babeleVo["folders"] = {}
                for key, value in json.load(f).items():
                    babeleVf["folders"][key] = value
                    babeleVfVo["folders"][key] = value
                    babeleVoVf["folders"][key] = value
                    babeleVo["folders"][key] = value
            continue

        # Fichiers de données
        data = fileToData(path + fpath)
        count[data["status"]] += 1

        if data["status"] == "aucune" or data["status"] == "auto-trad" \
                or data["status"] == "auto-googtrad" or data["status"] == "vide":
            continue
        elif "name" not in data or len(data["name"].get("en", "")) == 0 or len(data["name"].get("fr", "")) == 0:
            continue

        entry_id = data["name"].get("en")
        namefr = data["name"].get("fr", "") if data["name"].get("fr", "") != "" else data["name"].get("en")
        entryVF = {'name': namefr}
        entryVO = {'name': entry_id}
        entryVFVO = {'name': "%s" % entry_id if entry_id == namefr else ("%s (%s)" % (namefr, entry_id.split("|")[0]))}
        entryVOVF = {'name': "%s" % entry_id if entry_id == namefr else ("%s (%s)" % (entry_id.split("|")[0], namefr))}

        if "desc" in data and (descVF := data["desc"].get("fr", "")) != "":
            entryVF["description"] = descVF
            entryVO["description"] = descVF
            entryVFVO["description"] = descVF
            entryVOVF["description"] = descVF

        if "gm" in data and (gmVF := data["gm"].get("fr", "")) != "":
            entryVF["gm"] = gmVF
            entryVO["gm"] = gmVF
            entryVFVO["gm"] = gmVF
            entryVOVF["gm"] = gmVF

        if "fields" in p and "fields" in data:
            for k in p["fields"]:
                if k in data["fields"] and (fieldVF := data["fields"][k].get("fr", "").strip()) != "":
                    entryVF[k.lower()] = fieldVF
                    entryVO[k.lower()] = fieldVF
                    entryVFVO[k.lower()] = fieldVF
                    entryVOVF[k.lower()] = fieldVF

        if "lists" in p and "lists" in data:
            for k in p["lists"]:
                if len(data["lists"][k]) > 0 and len(data["lists"][k]["fr"]):
                    json_array = []
                    for item in data["lists"][k]["fr"]:
                        json_array += [{"value": item}]
                    entryVF[k.lower()] = json_array
                    entryVO[k.lower()] = json_array
                    entryVFVO[k.lower()] = json_array
                    entryVOVF[k.lower()] = json_array

        if "subMappings" in p and "submappings" in data:
            for sub_key, sub_value in p["subMappings"].items():
                if sub_key in data["submappings"]:
                    subItem = {}
                    for sub_entry_key, sub_entry_value in data["submappings"][sub_key].items():
                        subSubItem = {}
                        for field_key, field_value in data["submappings"][sub_key][sub_entry_key].items():
                            if len(field_value.get("fr", "")) > 0:
                                subSubItem[field_key] = field_value.get("fr", "")
                        if len(subSubItem) > 0:
                            subItem[sub_entry_key] = subSubItem
                    if len(subItem) > 0:
                        entryVF[sub_key] = {**subItem}
                        entryVO[sub_key] = {**subItem}
                        entryVFVO[sub_key] = {**subItem}
                        entryVOVF[sub_key] = {**subItem}

        if "items" in p and "items" in data:
            entryVF["items"] = {}
            entryVO["items"] = {}
            entryVFVO["items"] = {}
            entryVOVF["items"] = {}
            for key, item in data["items"].items():
                itemVF = {}
                itemVO = {}
                itemVFVO = {}
                itemVOVF = {}
                if "name" in item and item["name"].get("en") not in SKILLS and item["name"].get("fr", "") != "":
                    item_en = item["name"].get("en")
                    item_fr = item["name"].get("fr")
                    itemVF["name"] = item_fr
                    itemVO["name"] = item_en
                    itemVFVO["name"] = "%s" % item_en if item_en == item_fr else ("%s (%s)" % (item_fr, item_en.split("|")[0]))
                    itemVOVF["name"] = "%s" % item_en if item_en == item_fr else ("%s (%s)" % (item_en.split("|")[0], item_fr))
                if "desc" in item and (descVF := item["desc"].get("fr", "")) != "":
                    itemVF["description"] = descVF
                    itemVO["description"] = descVF
                    itemVFVO["description"] = descVF
                    itemVOVF["description"] = descVF
                if "subMappings" in p["items"] and "submappings" in item:
                    for sub_key, sub_value in p["items"]["subMappings"].items():
                        if sub_key in item["submappings"]:
                            if len(item["submappings"][sub_key].items()) > 0 and sub_key not in itemVF:
                                itemVF[sub_key] = {}
                                itemVO[sub_key] = {}
                                itemVFVO[sub_key] = {}
                                itemVOVF[sub_key] = {}
                            for sub_entry_key, sub_entry_value in item["submappings"][sub_key].items():
                                if len(item["submappings"][sub_key][sub_entry_key].items()) > 0 and sub_entry_key not in \
                                        itemVF[sub_key]:
                                    itemVF[sub_key][sub_entry_key] = {}
                                    itemVO[sub_key][sub_entry_key] = {}
                                    itemVFVO[sub_key][sub_entry_key] = {}
                                    itemVOVF[sub_key][sub_entry_key] = {}
                                for field_key, field_value in item["submappings"][sub_key][sub_entry_key].items():
                                    if len(field_value.get("fr", "")) > 0:
                                        itemVF[sub_key][sub_entry_key][field_key] = field_value.get("fr", "")
                                        itemVO[sub_key][sub_entry_key][field_key] = field_value.get("fr", "")
                                        itemVFVO[sub_key][sub_entry_key][field_key] = field_value.get("fr", "")
                                        itemVOVF[sub_key][sub_entry_key][field_key] = field_value.get("fr", "")
                if bool(itemVF):
                    entryVF["items"][key] = itemVF
                    entryVO["items"][key] = itemVO
                    entryVFVO["items"][key] = itemVFVO
                    entryVOVF["items"][key] = itemVOVF


        # Initialisation des pages
        if "pages" in p:
            entryVF["pages"] = {}
            entryVFVO["pages"] = {}
            entryVOVF["pages"] = {}
            entryVO["pages"] = {}

        babeleVf['entries'][entry_id] = entryVF
        babeleVfVo['entries'][entry_id] = entryVFVO
        babeleVoVf['entries'][entry_id] = entryVOVF
        babeleVo['entries'][entry_id] = entryVO

    if "pages" in p:
        path = "../data/%s-pages/" % p["id"]
        all_files = os.listdir(path)
        for fpath in all_files:
            data = fileToData(path + fpath)
            count[data["status"]] += 1
            if data["status"] == "aucune" or data["status"] == "auto-trad" \
                    or data["status"] == "auto-googtrad" or data["status"] == "vide":
                continue
            elif "name" not in data or len(data["name"].get("en", "")) == 0 or len(data["name"].get("fr", "")) == 0:
                continue
            babeleVf["entries"][data["journal"]]["pages"][data["name"]["en"]] = {"name": data["name"]["fr"], "text": data.get("desc", {}).get("fr", "")}
            babeleVo["entries"][data["journal"]]["pages"][data["name"]["en"]] = {"name": data["name"]["en"], "text": data.get("desc", {}).get("fr", "")}
            babeleVfVo["entries"][data["journal"]]["pages"][data["name"]["en"]] = {"name": "%s" % data["name"]["en"] if data["name"]["en"] == data["name"]["fr"] else ("%s (%s)" % (data["name"]["fr"], data["name"]["en"])), "text": data.get("desc", {}).get("fr", "")}
            babeleVoVf["entries"][data["journal"]]["pages"][data["name"]["en"]] = {"name": "%s" % data["name"]["en"] if data["name"]["en"] == data["name"]["fr"] else ("%s (%s)" % (data["name"]["en"], data["name"]["fr"])), "text": data.get("desc", {}).get("fr", "")}

    print("Statistiques: " + path[8:][:-1])
    print(" - Traduits: %d (officielle) %d (libre)" % (count["officielle"], count["libre"]))
    print(" - Changé: %d" % count["changé"])
    print(" - Non-traduits: %d" % (count["aucune"]))
    print(" - Vides: %d" % count["vide"])


all_packs = getPacks()
packs = all_packs["packs"]
for p in packs:
    module = p["module"] if "module" in p else "pf2e"
    key = "%s.%s" % (module, p["fileName"])
    packName = p["transl"]

    # Fichiers de destination
    babele = {'label': packName, 'entries': {}, 'mapping': {}}
    babeleVfVo = {'label': packName, 'entries': {}, 'mapping': {}}
    babeleVoVf = {'label': packName, 'entries': {}, 'mapping': {}}
    babeleVo = {'label': packName, 'entries': {}, 'mapping': {}}

    # Generate main pack JSON
    handlePack(p, babele, babeleVfVo, babeleVoVf, babeleVo)

    print(BABELE + key + ".json")
    with open(BABELE + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babele, f, ensure_ascii=False, indent=4)
    with open(BABELE_VF_VO + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babeleVfVo, f, ensure_ascii=False, indent=4)
    with open(BABELE_VO_VF + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babeleVoVf, f, ensure_ascii=False, indent=4)
    with open(BABELE_VO + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babeleVo, f, ensure_ascii=False, indent=4)

# Packs Folders
existing_folders = {}
if os.path.isfile("../data/folders.json"):
    existing_folders = json.load(open("../data/folders.json", 'r', encoding='utf8'))
for pack, values in existing_folders.items():
    data = {"entries": {}}
    data["entries"].update(values)
    with open("%s%s._packs-folders.json" % (BABELE, pack), 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    with open("%s%s._packs-folders.json" % (BABELE_VO, pack), 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    with open("%s%s._packs-folders.json" % (BABELE_VF_VO, pack), 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    with open("%s%s._packs-folders.json" % (BABELE_VO_VF, pack), 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
