# Module de traduction de Pathfinder 2 à destination de Foundry VTT

Ce module est destiné à être utilisé avec la plateforme virtuelle foundry VTT (https://foundryvtt.com/).  

Ce module permet de disposer de la traduction en français des données des objets contenus dans les packs du système Foundry et de la traduction en français de l'interface utilisateur du système Pathfinder 2e édition.

Pathfinder 2e édition est un jeu de rôle développé et commercialisé par Paizo. 

Le module permet de disposer :
- de l'interface utilisateur du système Pathfinder 2e en français, c'est à dire des explications dans les fenêtres de conversation, sur les feuilles de personnage, sur les monstres, etc.
- de la description des "items/objets" figurant dans les packs des compendiums anglophones également traduits en français. 

Le module permet de pouvoir disposer d'un affichage du nom de chaque objet de manière alternative, en vo, en vf, en vo-vf ou en vf-vo, la description restant en français. L'existence d'options dans le nommage facilite également le jeu sur des tables qui utilisent le français et l'anglais. Vous retrouverez les différentes options dans la partie "Utilisation sous Foundry" de ce document.

L'utilisation des noms doublés facilite la recherche et la prise en compte des noms qui peuvent être utilisés comme prédicats (mots-clefs servant de référence) par des modules qui permettent une automatisation.

Ledit module rend indispensable l'utilisation des modules suivants : 
1. Babele, ce module et ses propres dépendances sont nécessaires pour assurer la prise en charge de la traduction et la substitution lors de l'affichage en jeu. Il est développé par _Simone Riccisi_
2. PF2e system, ce module est celui qui assure la prise en charge par Foundry du système de jeu en lui-même avec ses règles, ses automatisations. Il est nécessaire pour charger les packs des compendiums de données du système de jeu qui sont traduites par le présent module
3. PF2e companion compendia, ce module est nécessaire pour charger les packs des compendiums de données propres aux différents compagnons (la créature de l'inventeur, le compagnon animal du druide ou du rôdeur, l'eidolon du conjurateur, les compagnons morts-vivants...). Il a été développé à part et sera probablement un jour mangé par le système 


**En aucun cas, Paizo ou Black Book Editions ne reconnaîssent, ne sont à l'origine ou ne sont les mécènes de ce module ou de ce projet.**

La gamme Pathfinder est une création de **Paizo Publishing**. Elle fait l'objet d'une publication officielle en français par **Black Book Editions**.

Le présent module est réalisé en application des licences Open Game License (ogl) et Pathfinder Community Use Policy (PCUP). 

Il est destiné à l'utilisation du système Pathfinder 2e avec le logiciel Foundry VTT 

L'utilisation de ce module pour permettre de jouer avec un produit commercialisé est strictement interdite. 

Ce module ne peut en aucun cas être une dépendance d'un autre module, à moins qu'il ne soit lui-même gratuit.

Vous ne pouvez notamment pas déclarer ce module dans la section 'dependencies' (ou toute section qui viendrait à lui être substituée) du fichier foundry module.json ou system.json de votre module, à moins qu'il ne soit lui même gratuit.

Par ailleurs, si vous entendez extraire ou utiliser les traductions des objets qui ont été réalisées par des fans à tout autre fin que l'utilisation de ce module et qui portent l'indicateur libre, vous devez **impérativement** :
- respecter les licences ogl et PCUP,
- avoir obtenu l'autorisation des traducteurs (Rectulo et Njini) et les créditer à ce titre.

Actuellement :
- Cheps a obtenu cette permission pour la création d'un [wiki](https://pf2e-srd.cheps.ovh/)  
- Fykk a obtenu cette permission pour le développement d'une application permettant de générer une feuille de personnage interactive

Vous ne pouvez en aucun cas utiliser les traductions libres réalisées gratuitement par des fans pour servir des fins commerciales.

- La traduction du Livre de base initial a été, pour l'essentiel, la reprise de la traduction officielle réalisée en français par Black Book Edition avec application des errata parus depuis lors et non encore portés en vf. Elle a été depuis modifiée pour appliquer la version de PF2 remaster.
- La description des monstres des Bestiaires 1 et 2 sont également issues de la traduction officielle réalisée par BBE mais là aussi affecté par les changements du remaster.
- La traduction des objets du compendium du Guide du Maître est également issue de la traduction officielle réalisée par BBE et affectée par le remaster.

Les traductions officielles utilisées ont été l'objet d'un travail complémentaire des contributeurs du module visant à supprimer les références des pages, mettre en forme les tables, insérer les liens nécessaires pour pouvoir naviguer dans le logiciel Foundry, pour réaliser les automatisations permises par le système de lancer de dé. Les fichiers ont également fait l'objet de l'insertion des erratas qui ont été publiés par Paizo, et ce, au fur et à mesure de leur intégration dans Foundry.

Certains textes ont aussi été légèrement repris par souci de cohérence et d'uniformisation dans l'utilisation de certains mots, lorsque la traduction n'a pas parue la mieux adaptée au concept ou que la traduction a paru erronée. 

Les erratas sont appliqués au fur et à mesure de leur parution en vo, ce qui peut conduire à un décalage avec la version officielle en français.

Le reste de la traduction des fichiers anglophones qui vous est proposée dans ce module est le fruit du travail passionné de membres de la communauté (Njini et Rectulo depuis l'origine, de Dwim pour les bestiaires des AP publiées en français chez BBE et Elsinin pour quelques objets de Treasure Vault). 

Ils se sont donnés pour objectif d'assurer la traduction des règles au rythme de leur parution en anglais pour permettre de jouer avec les campagnes parues en version originale et de prendre en charge toutes les règles non encore traduites en français.

## Note aux traducteurs 

* Les fichiers à traduire ou à corriger se trouvent dans le répertoire [data](data/).

* Chaque fichier correspond à une entrée traduite ou à traduire
  * Pour chaque fichier, vous disposez de plusieurs champs en anglais et en français,
  * Les textes d'origine en anglais doivent **rester inchangés** ! (ils permettent à un programme d'extraction des données à partir des fichiers anglophones d'effectuer automatiquement une comparaison entre le contenu de l'extraction précédente et une éventuelle mise à jour anglophone. Cette comparaison permet de signaler tous les changements, qu'il s'agisse d'évolutions ou dans l'automatisation, mais aussi les oublis ou typos et les éventuels erratas appliqués au fur et à mesure),
  * Voir [data/instructions.md](data/instructions.md) pour plus d'instructions concernant les instructions de traduction et de modifications pour ceux qui veulent contribuer.

* Pour voir l'état d'avancement des différentes traductions, vous pouvez consulter les fichiers qui se terminent par **.md** dans le répertoire data, classés par compendiums.

* À la différence des compendiums, les fichiers concernant l'UI/UX du système et les effets de règles qui sont rangés dans le dossier lang sont maintenus en utilisant **uniquement** le dépôt constitué sous crowdin. crowdin dispose de fonctionnalités avancées pour faire apparaître les changements entre deux versions des fichiers anglophones. Il ne faut pas opérer de changements en passant par le dépôt sous Gitlab mais uniquement par Crowdin.

## Utilisation sous Foundry

Pour appliquer les traductions dans votre système, il existe un prérequis logiciel. Il vous faut installer le module intitulé [Babele](https://gitlab.com/riccisi/foundryvtt-babele) qui a été créé par *Simone Riccisi*. Ce module réalise une cartogaphie des fichiers, repère les parties anglaises et permet ensuite de leur substituer les traductions proposées en français.

Une fois ce module et Babele installés et activés, il vous faut demander l'affichage en français dans les options. 

Instructions détaillées 
* Lorsque vous ouvrez le logiciel de VTT Foundry, allez dans l'onglet Modules et cliquez sur Installer un module
* Chercher Babele, puis **Installer** pour l'installer automatiquement
* Revenir dans l'onglet Module
* Chercher le module français [Pathfinder 2e] (fan made) ou bien entrer manuellement l'adresse du manifeste du module dans la barre d'adresse : `https://gitlab.com/pathfinder-fr/foundryvtt-pathfinder2-fr/-/raw/master/module.json`

Puis dans les options du module faire votre choix parmi les options
  * `vf(vo)`: noms au format "nom fr (nom en)" avec le contenu traduit en français
  * `vo(vf)`: noms au format "nom en (nom fr)" avec le contenu traduit en français
  * `vo`: noms conservés dans leur version originale (en) mais avec le contenu en français
  * `vf`: nom et description uniquement en vf
* Il ne vous reste qu'à en profiter !

L'utilisation vo(vf) et vf(vo) peut parfois amener des décalages inesthétiques dans l'affichage du système qui n'empêchent pas de jouer. 

Il vaut mieux utiliser les versions avec les noms dans les deux langues car plusieurs modules non localisés qui sont régulièrement utilisés codent des automatisations en utilisant des mots clés codés le plus souvent en langue anglaise. 

Si vous n'utilisez que la vf, vous ne profiterez pas de tous leurs effets.

Vous êtes invités à remonter les erreurs de traduction, les typos, les erratas sur le discord du projet. Une capture d'écran est la bienvenue.

## Ressources utiles

À partir des traductions, l'extraction complète au fil du temps et des publications a permis de disposer d'un [Glossaire](data/dictionnaire.md) qui vous donne le nom utilisé en anglais, le nom utilisé en français avec l'ID (l'identifiant du fichier qui permet d'insérer ailleurs un lien réutilisable pour faciliter la navigation). 

Un dictionnaire est aussi disponible en partant du français pour disposer de la traduction en anglais.

Compte tenu du "poids" des fichiers désormais, il faut les télécharger.

## Bugs de la vo
Les erreurs que vous trouvez dans les textes en vo peuvent être remontées sur le projet anglophone en créant une "issue" dans le dépôt du système en décrivant la phrase erronée et la phrase que vous proposez. Le mieux est de donner le numéro du fichier, suivie de sa traduction et indiquer en quoi il est incorrect, le tout dans la langue anglaise. Au pire, remontez l'information sur le discord du projet où un contributeur du système pourra vous relayer auprès des développeurs anglophones.

## Contribuer au wiki règles PF2

Pour cela, il faut commencer par vous créer un compte sur la plateforme Gitlab. C’est la plateforme qui héberge notre projet.

Vous aurez besoin d’un nom d’utilisateur et d’une adresse courriel que vous utiliserez : attention cette adresse électronique sera **« publique »**.

Après cela, vous avez deux solutions.

1.	Le plus simple : utiliser Gitlab et son éditeur de code intégré

C’est idéal pour la remontée de typos mais cela nécessite d’être connecté pour venir faire des modifications. C’est souvent plus lent pour travailler.
Vous avez alors la possibilité d’éditer directement un fichier ou d’ouvrir l’éditeur de code intégré qui est en réalité Visual Studio Code dans une version simplifiée et suffisante pour corriger des coquilles.

2.	Plus complexe mais plus confortable : une installation locale

La solution idéale pour contribuer durablement au projet, c’est de travailler en local. Vous n’avez pas besoin d’être connecté en permanence à Gitlab et à internet et vous pouvez travailler dans votre coin, dans l’avion, dans le train, en zone blanche et envoyer vos travaux lorsque vous vous connectez.

a.	Installer **Git**

Git est un logiciel de contrôle de version qui permet de travailler sur un projet sous la forme d’une sort de mille-feuille. Chaque intervention d'un utilisateur constitue une feuille. Cela permet qu’il ne se produise rien de grave puisqu'en cas d'erreur, il suffit de supprimer la feuille fautive sans toucher au reste. De même cela permet de regarder et de corriger facilement ce que font les utilisateurs.

Pour installer Git, c’est facile, il suffit de vous rendre sur https://git-scm.com/download

En fonction de votre système, il vous propose de le télécharger. Il devrait ensuite se lancer et il n’y a qu’à se laisser guider jusqu’au terme de l’installation.

Après l’installation de Git, vous devrez renseigner un nom/surnom et votre adresse de courriel. C’est une information importante car toutes les validations dans Git utilisent cette information et elle est indélébile dans toutes les validations que vous pourrez réaliser. Je le redis, l'adresse sera **publique**.

Pour cela, ouvrir Gitbash (cliquez sur l'icône Windows, tapez Git, vous verrez apparaître Git bash, cliquez dessus)

Dans la console, en remplaçant John Doe par vos propres noms et adresses tapez les instructions suivantes :
```$ git config --global user.name "John Doe"```
```$ git config --global user.email johndoe@example.com```


b.	Installer **Visual Studio Code (VSC)**

VSC est un éditeur de code qui constituera ensuite votre outil de travail et vous servira d’application pour accéder au projet et vous n’aurez plus besoin que de lui. Il va bénéficier du millefeuille de Git.

Une fois VSC téléchargé et installé, vous lancez VSC

Je vous suggère d’installer deux extensions immédiatement pour améliorer votre qualité de vie.

Pour cela, vous cliquez sur la roue crantée qui se trouve en principe en bas à gauche de votre écran puis vous sélectionnez ‘extension’
VSC ouvre alors une barre de recherche dans son magasin de modules. Dans la barre de recherches, vous cherchez en les rentrant, l’un après l’autre les titres des extensions suivantes :
- French language pack for Visual Studio Code
- Git extension pack qui est un pack d’extension regroupant les extensions que nous utilisons.

Vous les téléchargez. Il est possible que vous deviez relancer VSC pour qu’il passe en français et lance les extensions que vous venez de télécharger

Il ne vous reste plus qu’à télécharger dans VSC le ‘dépôt’ de données du wiki règles. 

Pour cela Ctrl-Maj+P – En réponse, le logiciel va ouvrir la ‘palette de commande’
Tapez Git clone puis la touche entrée – le logiciel vous demande l’adresse internet du dépôt que vous souhaitez cloner (c’est-à-dire télécharger sur votre ordinateur pour pouvoir disposer de son double en local)

Vous copiez l’adresse suivante : https://gitlab.com/pathfinder-fr/foundryvtt-pathfinder2-fr.git

VSC vous demande de définir à quel endroit sur votre ordinateur vous souhaitez héberger en local les données sur votre ordinateur. Les données pèsent environ 800 Mo. Si vous avez une partition de votre disque dur, je vous suggère de le mettre dans la partie dans laquelle vous conservez des données.

Une fois que c’est fait, un téléchargement va avoir lieu qui peut durer quelques dizaines de secondes en fonction de la qualité de votre connexion.

Quelle que soit l’option que vous avez choisie, vous êtes désormais prêts à contribuer !

c. Demander à être admis sur le projet

Le projet a été protégé, seuls ceux qui ont un droit d'écriture peuvent le modifier. Vous pouvez demander sur le discord Pathfinder-fr à rejoindre le projet en vous signalant auprès de rectulo. Il faudra que vous donniez le nom que vous utilisez dans Gitlab pour qu'on puisse vous trouver.


À jour au 5 janvier 2024